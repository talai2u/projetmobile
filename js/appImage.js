$(document).ready(function(){
	
		$.ajax({
		            url: "http://yns.alwaysdata.net/api/api/images/"+$.urlParam("id"),
		            type: 'GET',
		            dataType: 'json',
		            timeout: 1000,
		            data:'',
		            error: function(){console.log('error serveur'); },
		            success: placeImages
	    });
	
	
	$("#index").click(function(){
    	window.location.href= "index.html";
    });

});

function placeImages(data){
	var main = $("#main");
		for(i in data){
			var city = data[i];

			if($.type(city) == "object"){

				div = $("<div id='links' class='col-xs-4 col-sm-4'></div>");
				a = $("<a data-gallery></a>");
				
				// image
				img = $("<img width='100' class='img-responsive'/>");
				img.attr("alt", city.name )
				img.attr("src", city.url )

				// Link
				a.attr("title", city.name);
				a.attr("href", city.url);

				a.append(img);
				div.append(a);

				
				var lastRow = main.find(".row").last();

				if(lastRow.children().length>0){
					if(lastRow.children().length == 3 ){
						var row =$("<div class='row thumbnail'></div>");
						row.append(div);
						main.append(row);
					}
					else if(lastRow.children().length < 3){
						lastRow.append(div);
					}
					
				}
				else{
					var row =$("<div class='row thumbnail'></div>");
					row.append(div);
					main.append(row);
				}
		}else{
			row = $("<div style='text-align:center;' class='row'></div>");
			h3 = $("<h3>Aucun résultat</h3>");
			row.append(h3);
			main.append(row);
			return false;

		}
					
	}

	
}

$.urlParam = function(name){
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results==null){
	       return null;
	    }
	    else{
	       return results[1] || 0;
	    }
}

function dirname(path) {
    return path.replace(/\\/g, '/')
        .replace(/\/[^\/]*\/?$/, '');
}


