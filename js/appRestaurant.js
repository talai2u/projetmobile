$(document).ready(function(){
	
		$.ajax({
		            url: "http://yns.alwaysdata.net/api/api/restaurants/"+$.urlParam("id"),
		            type: 'GET',
		            dataType: 'json',
		            timeout: 1000,
		            data:'',
		            error: function(){console.log('error serveur'); },
		            success: placeHotels
	    });
	
	
	$("#index").click(function(){
    	 window.location.href= "index.html";
    });

});

function placeHotels(data){
	$("#hotelContent").empty();
	
	title = $("<h4>Restaurants sur "+$.urlParam("city")+"</h4>");
	$("#hotelContent").append(title);
	

	for(i in data){
		var hotel = data[i];
		
		if($.type(hotel) == "object"){

			row = $("<div class='row hotel_content'></div>");
			
			divImg = $("<div class='col-xs-4'></div>");
			img = $("<img class='thumbnail img-responsive hotel_img'/>");
			img.attr("src",hotel.image)
			divImg.append(img);
			row.append(divImg);

			divInfo = $("<div class='col-xs-8 '></div>");

			rowInfo = $("<div class='row thumbnail '></div>");
			strong = $("<strong></strong>")
			strong.append(hotel.name);
			rowInfo.append(strong);
			divInfo.append(rowInfo);

			rowInfo = $("<div class='row info'></div>");
			strong = $("<p><strong>Adresse : </strong>"+hotel.address+"</p>");
			rowInfo.append(strong);
			divInfo.append(rowInfo);

			rowInfo = $("<div class='row info'></div>");
			strong = $("<p><strong>Téléphone : </strong>"+hotel.phone+"</p>");
			rowInfo.append(strong);
			divInfo.append(rowInfo);

			

			row.append(divInfo);

			$("#hotelContent").append(row);

		}else{
			row = $("<div style='text-align:center;' class='row hotel_content'></div>");
			h3 = $("<h3>Aucun résultat</h3>");
			row.append(h3);
			$("#hotelContent").append(row);
			return false;

		}
	}


	
}

$.urlParam = function(name){
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results==null){
	       return null;
	    }
	    else{
	       return results[1] || 0;
	    }
}

function dirname(path) {
    return path.replace(/\\/g, '/')
        .replace(/\/[^\/]*\/?$/, '');
}


